#!/usr/bin/env ruby

require './String'
require 'open-uri'
require 'nokogiri'

class WebPage
	def initialize(url, content, cert)
		@url = url
		@content = content
		@cert = cert
		@links = []
	end
	# escreve a página para um arquivo
	def savePage
		f = File.open("../pages/#{@url.to_filename}.html", 'w') {|f| f.write(@content) }
	end

	def saveCertificate
		if @cert
			f = File.open("../certificates/#{@url.to_filename}.pem", 'w') {|f| f.write(@cert) }
			f = File.open("../certificates/txt/#{@url.to_filename}.txt", 'w') {|f| f.write(@cert.to_text) }
		end
	end
	# retorna um vetor com todos os links para onde WebPage aponta
	def links
		if !@links.empty?
			return @links
		else
			singleLink = Hash.new

			singleLink["#{@url}"] = true

			doc = Nokogiri::HTML(@content)
			
			if doc
				doc.css('a').map do |link|
					temp = linkMaker(link['href'])

					if temp && link['href'] != '#' && !singleLink["#{temp}"]
						singleLink["#{temp}"] = true
						@links.push(temp)
					end
				end
			end

			return @links;
		end
	end

	private
	def linkMaker(link)
		return false if !link
		# para casos de href = ./
		return @url if link == "./"
		# para os casos que falta o dominio no url, normalmente começado por / ou ./
		return @url + link if link.match(/^\//)
		return @url + link.slice!(1..link.length-1) if link.match(/^\.\//)
		return link
	end
end
