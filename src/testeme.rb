#!/usr/bin/env ruby

require './Connection'
require 'thread'
require 'monitor'

puts 'Nenhum parametro passado!\n' if ARGV.length <= 0
puts 'Falta de parametro passado!\n' if ARGV.length < 2 && ARGV.length > 0
puts 'Modo correto: ./main.rb url profundidade\n' if ARGV.length < 2
exit if ARGV.length < 2

# Total de threads = MAXTHREAD + 1 (mainThread)
MAXTHREAD = 7
# vetor de threads
$thread = []
# vetor para acumular vetores de links, cada página visitada retorna um um vetor de links
$url = []
# monitor para controle do vetor
$url.extend(MonitorMixin)
# variável para controle de profundidade
$depth = ARGV[1].to_i
# lista com os links não visitados da profundidade atual, já inicializado com a url passada como parâmetro
$unvisited = [].push(ARGV[0])
# monitor para controle da lista
$unvisited.extend(MonitorMixin)
# condição para wait busy
$cond = $unvisited.new_cond
# hash para controle de links visitados
$visited = Hash.new
# monitor para controle do hash
$visited.extend(MonitorMixin)
# função para visitar os links na fila
def run
	temp = ''
	# enquanto tiver links na lista, remove o primeiro e visita
	if !$unvisited.empty?
		$unvisited.synchronize do
			temp = $unvisited.shift
		end

		return false if !temp || temp == ''

		connection(temp)
	elsif $depth > 0 && !$url.empty?
		$url.synchronize do
			$url.each do |links|
				links.each do |link|
					$unvisited.synchronize do
						# adiciona cada link na fila
						$unvisited << link
						# para cada link da signal na condicional para liberar threads que estejam trancadas na condição
						$cond.signal
					end
				end
			end
			# limpa a lista
			$url.drop($url.length)
			# decrementa a profundidade
			$depth -= 1
		end
	end
end

def connection(link)
	# remove a ultima / de cada link para controle da hash, pois www.domain.com == www.comain.com/ 
	link = link.slice!(0..link.length-2) if link.match(/\/$/)
	# visita apenas se o link não foi visitado
	if link && !$visited["#{link}"]
		# cria o socket e faz a conexão
		socket = Connection::new(link)
		# marca o link como visitado
		$visited.synchronize do
			$visited["#{link}"] = true
		end
		# recebe a página
		page = socket.page
		# salva a página html
		page.savePage
		# salva o certificado ssl
		page.saveCertificate
		# adiciona links quando a profundidade for maior que zero
		if $depth > 0
			puts "\n\tLinks da página #{link}:\n\n"
			page.links.each	{ |link| p link}

			$url.synchronize do
				$url.push(page.links)
			end
		end
	end	
end

mainThread = Thread.new do
	loop do
		if $unvisited.empty? && $depth == 0
			puts "Main Thread morta\n"
			mainThread.exit
			break
		end

		run()
	end
end

MAXTHREAD.times do |i|
	$thread[i] = Thread.new {
		sleep(5)
		loop do
			if $unvisited.empty? && $depth == 0
				puts "Thread #{i} morta\n"
				$thread[i].exit
				break
			end

			$unvisited.synchronize do
				$cond.wait_while { $unvisited.empty? }
			end

			run()
		end
	}
end

mainThread.join

$thread.each { |t| t.join }