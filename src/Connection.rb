#!/usr/bin/env ruby

require 'rubygems'
require 'openssl'
require 'uri'
require 'socket'
require './WebPage'

class Connection
	def initialize(url, port = 80, debug = true)
		@port = port
		@uri = URI.parse(url)

		@cert = nil

		#pega o tipo de conexão HTTP ou HTTPS
		@scheme = @uri.scheme
		
		#pega o nome do servidor em que a página se encontra
		@domain = @uri.host
		
		#pega o local dentro do servidor onde página se encontra
		if @uri.path == ""
			@path = "/"
		elsif @uri.path.match(/\/$/)
			@path = @uri.path.slice!(0..@uri.path.length-2)
		else
			@path = @uri.path
		end

		@query = @uri.query

		@fragment = @uri.fragment
		
		#gera o pedido
		@request = "GET #{@path}"
		@request<< "?#{@query}" if @query
		@request<< "##{@fragment}" if @fragment
		@request<< " HTTP/1.1\r\n"
		
		@request<< "Host: #{@domain}:#{port}\r\n\r\n"
		
		#caso esteja em Debug, escreve o valor do pedido
		debug_text = "\n#{@request}"
		#Faz a conexao na porta 443 para pegar o certificado
		begin
			connection = TCPSocket.new(@domain, @port)
		rescue
			puts "Error: #{$!} !!!\n"
		else
			if @scheme == 'https'
				ssl_connection = TCPSocket.new(@domain, 443)

				ctx = OpenSSL::SSL::SSLContext.new
  				ctx.set_params(ca_path: "/etc/ssl/certs/", verify_mode: OpenSSL::SSL::VERIFY_PEER, verify_depth: 5)

				ssl_client = OpenSSL::SSL::SSLSocket.new ssl_connection, ctx

				ssl_client.connect

				@cert = OpenSSL::X509::Certificate.new(ssl_client.peer_cert)

				ssl_client.sysclose
				ssl_connection.close
				
				#informacoes do certificado
				debug_text<< "CERTIFICADO AUTO ASSINADO!!!\n\n" if !@cert.issuer.to_s["CA"]

				debug_text<< "Cetificado:\n"
				debug_text<< "Emissor: #{@cert.issuer}\n"
				debug_text<< "Criado em: #{@cert.not_before}\n"
				debug_text<< "Válido até: #{@cert.not_after}\n"
				debug_text<< "Portador: #{@cert.subject}\n"
			end

			puts debug_text if debug

			connection.print(@request)

			answer = connection.read

			connection.close

			@header, @body = answer.split("\r\n\r\n", 2)
		end
	end

	# retorna pagina web
	def page
		return WebPage::new(@uri.to_s, @body, @cert)
	end
	# retorna o cabeçalho da resposta HTTP
	def header
		return @header
	end
end

#Connection::new(ARGV[0] ||'https://www.facebook.com')