Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            01:00:00:00:00:01:3d:5f:02:c1:70:24:57:48
    Signature Algorithm: sha1WithRSAEncryption
        Issuer: O=Cybertrust Inc, CN=Cybertrust Public SureServer SV CA
        Validity
            Not Before: Mar 12 14:05:39 2013 GMT
            Not After : Mar 12 14:05:39 2014 GMT
        Subject: C=US, ST=CALIFORNIA, L=Cupertino, O=Apple Inc., CN=www.apple.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:a8:04:8b:1d:d8:48:0d:6e:88:de:3f:f6:bc:58:
                    66:1a:9b:0e:6a:6e:83:24:68:ea:02:a2:f1:73:2c:
                    05:fa:bc:b1:2f:49:62:27:37:ab:c3:d2:9b:33:42:
                    56:63:d2:67:2a:10:57:df:c6:24:67:42:29:de:70:
                    56:3b:37:bc:23:06:08:f4:fe:07:79:e1:11:6d:3d:
                    e6:8f:c0:ba:9b:b7:1e:b6:84:42:b3:c6:d7:90:1f:
                    25:ac:57:00:75:bd:f9:c0:e5:4d:46:74:7c:3a:d7:
                    78:0c:0c:91:bc:86:65:bd:da:e3:3e:94:18:ba:31:
                    65:b5:94:6f:ed:47:22:b8:2e:ae:d1:a4:f3:2a:0e:
                    26:00:2d:58:bf:79:59:3b:5f:85:6d:a3:37:76:fc:
                    8a:30:22:12:2f:26:55:24:61:54:7a:f5:aa:ae:e9:
                    83:e6:3c:05:3e:6c:48:b9:78:8d:a8:32:72:e1:f3:
                    97:c9:a3:fb:1f:69:74:cc:b1:37:7f:79:04:5f:18:
                    27:03:58:ca:35:e4:a4:a0:6b:4a:fe:3d:31:90:f7:
                    96:6a:1c:73:4c:52:89:86:58:f6:b9:9f:45:0d:20:
                    59:37:22:4b:89:c4:ee:1e:0f:57:0c:ba:1f:9d:85:
                    47:f9:99:4b:3a:35:52:35:19:8e:4b:1a:4e:17:47:
                    b3:0f
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Authority Key Identifier: 
                keyid:04:98:60:DF:80:1B:96:49:5D:65:56:2D:A5:2C:09:24:0A:EC:DC:B9

            X509v3 CRL Distribution Points: 

                Full Name:
                  URI:http://crl.omniroot.com/PublicSureServerSV.crl

            X509v3 Subject Key Identifier: 
                91:24:BD:87:DB:6D:F4:44:CA:39:3E:98:FA:EF:29:A9:B3:86:02:55
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage: 
                TLS Web Server Authentication, TLS Web Client Authentication
            Netscape Cert Type: 
                SSL Client, SSL Server
    Signature Algorithm: sha1WithRSAEncryption
         2c:79:a6:4d:e8:bb:34:f0:80:fd:0e:26:64:5c:ad:2c:05:d0:
         9c:71:ba:03:46:9a:fb:b7:cb:99:25:73:1d:16:f3:88:7f:50:
         0b:5d:e5:5c:a0:98:d6:09:00:29:8f:3d:8c:c2:2d:b4:9e:47:
         31:ac:e1:73:61:8d:04:67:0e:74:c2:4b:41:0a:5b:4f:13:e5:
         44:70:ea:15:a6:c7:32:5c:7d:0f:a3:ee:37:69:e6:f3:81:34:
         fc:7f:90:d0:ef:f9:1c:c3:ff:a0:d0:d9:25:c7:ef:1a:32:c5:
         26:61:8d:f5:e2:62:d5:ce:16:8f:40:af:b3:57:18:bc:18:b8:
         69:2d:20:7a:97:62:fe:66:42:ad:4d:64:45:99:21:f3:43:fb:
         fb:d0:e3:f5:ee:74:b0:d2:29:57:94:03:00:81:a5:ba:cb:67:
         5b:c1:3b:ee:a8:02:94:12:6f:f5:3b:2d:03:1a:66:83:2d:b6:
         e0:5d:40:63:52:2f:ff:5d:b7:a7:49:d2:e8:1b:3c:24:50:64:
         63:f0:be:90:47:f9:61:61:ac:e5:8f:bd:7c:a8:70:1a:07:c0:
         10:d0:6a:1f:f5:f7:18:f3:e0:d9:3b:03:bf:87:fb:8e:f2:3a:
         a0:6e:38:90:e5:11:0f:e1:cf:86:c9:8e:90:be:11:13:5b:03:
         e0:ef:d9:2d
